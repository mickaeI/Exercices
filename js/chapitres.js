// AJAX
// Requête vers liens du JSON
var requestURL = 'data/chapitres.json';
var request = new XMLHttpRequest();
request.open('GET', requestURL);
// Identification du type de données
request.responseType = 'json';
request.send();

request.onload = function() {
    var articles = request.response;
    listerArticles(articles);
}

class Article {
    constructor(titre, paragraphe) {
        this.titre = titre;
        this.paragraphe = paragraphe;
    }
    creerArticle() {
        var monArticle = document.createElement('article');
        var monTitre = document.createElement('h1');
        var monParagraphe = document.createElement('p');

        var maSection = document.querySelector("section");
        maSection.appendChild(monArticle);
        monArticle.appendChild(monTitre);
        monArticle.appendChild(monParagraphe);

        monTitre.textContent = this.titre;
        monParagraphe.textContent = this.paragraphe;
    }
}

function listerArticles(jsonObj) {
    var  listeArticles= jsonObj['articles'];

    for(let article of listeArticles){
        var articleObjet= new Article(article.titre, article.paragraphe); 
        articleObjet.creerArticle();
    }
}

// CE QUI A AU DESSUS REMPLACE CELA :


//var article1 = new Article("titre 1", "p 1"); 
//article1.creerArticle();
/*
var maDiv = document.createElement("div");
var monTitre = document.createElement("h1");
var monParagraphe = document.createElement("p");
monTitre.innerText = "Bienvenue !";
monParagraphe.innerText = "Bienvenue sur mon site ! Plein de lorem ipsum...";
maDiv.appendChild(monTitre);
maDiv.appendChild(monParagraphe);
document.body.appendChild(maDiv);

var maDiv1 = document.createElement("div");
var monTitre1 = document.createElement("h1");
var monParagraphe1 = document.createElement("p");
monTitre1.innerText = "Chapitre 1 : un chapitre pour débuter";
monParagraphe1.innerText = "Paragraphe chapitre 1";
maDiv1.appendChild(monTitre1);
maDiv1.appendChild(monParagraphe1);
document.body.appendChild(maDiv1);

var maDiv3 = document.createElement("div");
var monTitre3 = document.createElement("h1");
var monParagraphe3 = document.createElement("p");
monTitre3.innerText = "Chapitre 2 : un autre chapitre";
monParagraphe3.innerText = "Paragraphe chapitre 2";
maDiv3.appendChild(monTitre3);
maDiv3.appendChild(monParagraphe3);
document.body.appendChild(maDiv3);

var maDiv4 = document.createElement("div");
var monTitre4 = document.createElement("h1");
var monParagraphe4 = document.createElement("p");
monTitre4.innerText = "Chapitre 3 : encore un chapitre";
monParagraphe4.innerText = "Paragraphe chapitre 3";
maDiv4.appendChild(monTitre4);
maDiv4.appendChild(monParagraphe4);
document.body.appendChild(maDiv4);
*/
