class Robot{
    constructor(){
        // Forme générale
        this.tete = "arrondi";
        this.corps = "trapeze";
        this.brasGauche = "articule";
        this.brasDroit = "articule";
        this.jambes = "5_chenilles";
        // Détails
        this.antenne = "1";
        this.oeil = "1";
        this.bouche = "5_points";
        this.symbole = "rouages";
        // Couleurs
        this.couleurTete = "jaune";
        this.couleurCorps = "turquoise";
        this.couleurBras = "noir_blanc";
        this.couleurJambes = "orange jaune";
    }
    // Fonctions de mon robot :
    capter(){
    }
    avancer(){
    }
    tourner(){
    }
    calculerTrajet(){
    }
}
class ModifsRobot extends Robot {
    constructor(){
        super();
        // Nouvelles propriétés du robot
        this.brasDroit = "grappin";
        this.jambes = "ressortsRoulettes"
    }
    // Nouvelles fonctions de mon robot :
    sauter(){
    }
    activerGrappin(){
    }
    rentrerGrappin(){
    }
    saisirRebord(){
    }
    saisirFruit(){
    }
}
var Robot1 = new Robot();


