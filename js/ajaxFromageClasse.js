// Eléments Html en variables
var header = document.querySelector('header');
var section = document.querySelector('section');
// Requête vers liens du JSON
var requestURL = 'data/ajaxFromage.json';
var request = new XMLHttpRequest();
request.open('GET', requestURL);
// Identification du type de données
request.responseType = 'json';
request.send();

request.onload = function() {
    var fromages = request.response;
    populateHeader(fromages);
    listerFromages(fromages);
}
// Générer le titre 
function populateHeader(jsonObj) {
    var myH1 = document.createElement('h1');
    myH1.textContent = jsonObj['titre'];
    header.appendChild(myH1);
}
// Classe fromage, génére les articles des fromages
class Fromage {
    constructor(nom, type, pays, classement) {
        this.nom = nom;
        this.type = type;
        this.pays = pays;
        this.classement = classement;
    }
    creerFromage() {
        var myArticle = document.createElement('article');
        var myH2 = document.createElement('h2');
        var myPara1 = document.createElement('p');
        var myPara2 = document.createElement('p');
        var myPara3 = document.createElement('p');
        var myPara4 = document.createElement('p');
    
        // myH2.textContent = listeFromages[i].name;
        myPara1.textContent = 'Nom : ' + this.nom;
        myPara2.textContent = 'Type : ' + this.type;
        myPara3.textContent = 'Pays :' + this.pays;
        myPara4.textContent = 'Classement :' + this.classement;
    
        myArticle.appendChild(myH2);
        myArticle.appendChild(myPara1);
        myArticle.appendChild(myPara2);
        myArticle.appendChild(myPara3);
        myArticle.appendChild(myPara4);
    
        section.appendChild(myArticle);
    }
}

function listerFromages(jsonObj) {
    var listeFromages = jsonObj['fromages'];

    /* Méthode 1 : bourrin
    for (var i = 0; i < listeFromages.length; i++) {
        var fromage = new Fromage(listeFromages[i].nom, listeFromages[i].type, listeFromages[i].pays, listeFromages[i].classement); 
        fromage.creerFromage();
    }
    */
    // Méthode 2 : plus classe
    for(let fromage of listeFromages){
        let fromageObjet= new Fromage(fromage.nom, fromage.type, fromage.pays, fromage.classement); 
        fromageObjet.creerFromage();
    }

}
