
// Fonction principale
function nombreMystere(){
    // Variables de bases liées à la difficulté
    var min = 1;
    var max = 100;
    var nbEssai = 0;
    var rejouer = "o";
    // Générer le nombre à trouver
    function nombreAleatoire(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    var vraiNombre = nombreAleatoire(min,max);
    console.log("Nombre à trouver : "+vraiNombre);

    // Tant que l'utilisateur n'a pas trouver le bon nombre
    while (essaiNombre!=vraiNombre && rejouer=="o") {
        // Récupérer le nombre essayé par le joueur
        var essaiNombre = prompt("Essayez un nombre compris entre "+min+" et "+max+" (ce dernier est exclu).\nIl vous avez essayé "+nbEssai+" fois.");
        console.log("Nombre essayé : "+essaiNombre);
        // Tester le nombre essayé et guider le joueur
        if (essaiNombre < vraiNombre) {
            alert("C'est plus grand !");
        }
        else if (essaiNombre > vraiNombre) {
            alert("C'est plus petit !");
        }
        // Si il n'est ni plus petit ni plus grand alors il est bon, c'est gagné
        else {
            alert("C'est gagné !");
            rejouer = prompt("Voulez-vous rejouer ? (o/n)");
        }
        // J'ajoute un essai a chaque fois qu'il ... essaye
        nbEssai++;
    }
    // S'il veut rejouer je créer un nouveau nombre aléatoire
    if (rejouer=="o"){
        nombreMystere();
    }
    // Sinon il se casse de mon jeu
    else {
        alert("Dommage à la prochaine !");
    }
    
}
    

