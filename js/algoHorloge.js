function horloge() {

    function majHeure() {
        // Récupérer l'heure actuelle
        var date = new Date();
        var time = date.getHours() * 3600 +
            date.getMinutes() * 60 +
            date.getSeconds() * 1 +
            date.getMilliseconds() / 1000;
        // Convertir les heures en degrés
        var heures = time / 60 / 12 * 6;
        var minutes = time / 60 * 6;
        var secondes = time * 6;
        var date = date.getDate();
        // Modifier les classes des aiguilles afin de les faire tourner
        document.querySelector('.heure').style.transform = 'rotate(' + heures + 'deg)';
        document.querySelector('.minute').style.transform = 'rotate(' + minutes + 'deg)';
        document.querySelector('.seconde').style.transform = 'rotate(' + secondes + 'deg)';
        document.querySelector('.date').innerHTML = date;
    }
    // Recommencer toute les 50ms afin que cela soit fluide
    setInterval(majHeure, 100);
}


