// Elements de contrôle et affichage
var boutonValider = document.querySelectorAll('input')[2];
// Variables
var bonIdentifiant = "bonIdentifiant";
var bonMotDePasse = "bonMotDePasse";
var identifiantAdmin = "identifiantAdmin";
var motDePasseAdmin = "motDePasseAdmin";
var nombreEssai = 0;
// Tests
var testInput = function (){
    // Elements de contrôle et affichage testés à chaque fois
    var inputIdentifiant = document.querySelectorAll('input')[0].value;
    var inputMotDePasse = document.querySelectorAll('input')[1].value;

    if (nombreEssai < 4){
        if (inputIdentifiant==bonIdentifiant && inputMotDePasse==bonMotDePasse){
            alert("Vous êtes connecté en tant qu'utilisateur lambda !");
        }
        else if (inputIdentifiant==identifiantAdmin && inputMotDePasse==motDePasseAdmin){
            alert("Vous êtes connecté en tant qu'admin !")
        }
        else {
            alert("Mot de passe ou identifiant incorrect !");
        }
        nombreEssai++;
    }
    else {
        alert("Vous ne passerez pas !");
        boutonValider.style.display = 'none';
    }
}

var reset = function (){
    document.querySelectorAll('input')[0].value="";
    document.querySelectorAll('input')[1].value="";
}
// Actions des boutons
boutonValider.addEventListener('click', testInput, reset);

/*

EX3
====
A: 4
B: 6

EX4
====
5.8

EX5
====
A: 3
B: 5
C: 8
A: 2
C: 2

EX6
====
A: 3
B: 7
A: 9
B: 5

EX7
====
A: 3
B: 5
A: 5
B: 5

EX8
====
A = 5
B = 5
-----
A = 3
B = 5
C = B
B = A
A = C
-----
A = 5
B = 3

EX9
====

123456

EX10
====

C = 5

EX11
====

if (age == 18 || age == 19){
    alert("Junior");
}
if (age >= 20 && age <= 22){
    alert("Espoir");
}
if (age >= 23 && age <= 39){
    alert("Sénior");
}
if (age >= 40){
    alert("Vétéran");
}

EX12
====

for (i=0; i<10; i++){
    div.textContent = i;
}

for (i=10; i>0; i--){
    div.textContent = i;
}

EX13
====

for (i=0; i<10; i++){
    i++;
    div.textContent = i;
}

EX14
====

var somme = 0;
var tailleTableau = 100;

for (i=0; i<tailleTableau; i++){
    
    somme = somme + ...('tableau')[i];
}
------------------------------------------
var petiteCase = 100000000000000000000000000; 
var tailleTableau = 100;

for (i=0; i<tailleTableau; i++){
    if ( ...('tableau')[i] < petiteCase )
    petiteCase = ...('tableau')[i];
}

------------------------------------------
var grandeCase = 0; 
var tailleTableau = 100;

for (i=0; i<tailleTableau; i++){
    if ( ...('tableau')[i] > grandeCase )
    grandeCase = ...('tableau')[i];
}


EX15
====

utiliser "===" pour tester le type
*/
