var header = document.querySelector('header');
var section = document.querySelector('section');

var requestURL = 'data/ajaxFromage.json';

var request = new XMLHttpRequest();

request.open('GET', requestURL);

request.responseType = 'json';
request.send();

request.onload = function() {
    var superHeroes = request.response;
    populateHeader(superHeroes);
    showHeroes(superHeroes);
}

function populateHeader(jsonObj) {
    var myH1 = document.createElement('h1');
    myH1.textContent = jsonObj['titre'];
    header.appendChild(myH1);
  }

  function showHeroes(jsonObj) {
    var listeFromages = jsonObj['fromages'];
        
    for (var i = 0; i < listeFromages.length; i++) {
      var myArticle = document.createElement('article');
      var myH2 = document.createElement('h2');
      var myPara1 = document.createElement('p');
      var myPara2 = document.createElement('p');
      var myPara3 = document.createElement('p');
      var myPara4 = document.createElement('p');
  
      myH2.textContent = listeFromages[i].name;
      myPara1.textContent = 'Nom : ' + listeFromages[i].nom;
      myPara2.textContent = 'Type : ' + listeFromages[i].type;
      myPara3.textContent = 'Pays :' + listeFromages[i].pays;
      myPara4.textContent = 'Classement :' + listeFromages[i].classement;
  
      myArticle.appendChild(myH2);
      myArticle.appendChild(myPara1);
      myArticle.appendChild(myPara2);
      myArticle.appendChild(myPara3);
      myArticle.appendChild(myPara4);
  
      section.appendChild(myArticle);
    }
  }

