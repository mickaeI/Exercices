// Fonction principale
function nombreMystere() {
    // Variables de bases liées à la difficulté
    var min = 1;
    var max = 100;
    var nbEssai = 0;
    var retour;
    var vraiNombre = prompt("Taper le nombre à faire deviner à la machine : ");
    console.log("Nombre à trouver : " + vraiNombre);

    // Premier essai est toujours la moitié de la limite
    var essaiPrecedent = max/2;
    var essaiProchain;
    
    // Tant que la machine n'a pas trouver le bon nombre ======================================================
    while (essaiPrecedent != vraiNombre) {
        // Tester le nombre essayé et guider la machine
        if (essaiPrecedent < vraiNombre) {
            alert("C'est plus grand !");
            essaiProchain = essaiPrecedent + essaiPrecedent / 2;
            essaiProchain = Math.ceil(essaiProchain);
        }
        else if (essaiPrecedent > vraiNombre) {
            alert("C'est plus petit !");
            essaiProchain = essaiPrecedent / 2;
            essaiProchain = Math.ceil(essaiProchain);
        }
        else {
            alert("C'est gagné !");
        }
        console.log("Essai Précédent : " + essaiPrecedent);
        console.log("Essai Prochain : " + essaiProchain);
        essaiActuel =
        essaiPrecedent = essaiProchain;
        // J'ajoute un essai a chaque fois qu'il ... essaye
        nbEssai++;
    }
    // ============================================================================================================
}



