class MonCanvas {
    constructor (largeurCanvas, hauteurCanvas) {
        this.largeurCanvas = largeurCanvas;
        this.hauteurCanvas = hauteurCanvas;
        this.creerCanvas();
    }
    creerCanvas() {
        let elementCanvas = document.createElement("canvas");
        elementCanvas.width = this.largeurCanvas;
        elementCanvas.height = this.hauteurCanvas;
        document.body.appendChild(elementCanvas);
        this.ctx = elementCanvas.getContext('2d');
    }
    dessinerFruit(){
        this.imageFruit.onload = () => {
            this.ctx.drawImage(this.imageFruit, this.x, this.y, this.largeurImage, this.hauteurImage);
            console.log(this);
        }
    }
}

class MonFruit extends MonCanvas {
    constructor (largeurCanvas, hauteurCanvas, src, largeurImage, hauteurImage){
        super(largeurCanvas, hauteurCanvas);
        this.x = window.innerWidth/2.7;
        this.y = window.innerHeight/6;
        this.imageFruit = new Image();
        this.imageFruit.src = src;
        this.largeurImage = largeurImage;
        this.hauteurImage = hauteurImage;

        this.dessinerFruit();
    }
}
// INIT
var poire = new MonFruit(window.innerWidth, window.innerHeight, "img/poire.png", window.innerWidth/4, window.innerWidth/4);
