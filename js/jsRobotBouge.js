/* Je déclare mes variables ----------------------------------------------------------------------------- */
var lacet1 = document.querySelector('#piedsGauche').innerHTML;
var lacet2 = document.querySelector('#piedsDroit').innerHTML;
var cou = document.querySelectorAll('article')[1];
var brasGauche = document.querySelectorAll('article')[2];
var tronc = document.querySelectorAll('article')[3];
var brasDroit = document.querySelectorAll('article')[4];
var piedsGauche = document.querySelector('#piedsGauche');
var piedsDroit = document.querySelector('#piedsDroit');
var tete = document.querySelector('#tete');
var bulle = document.querySelector('#bulle');
var pieds = document.querySelector('.pieds');
var bouche = document.querySelector('#bouche');
var bouche2 = document.querySelector('#bouche2');
var jambeGauche = document.querySelector('#jambeGauche');
var jambeDroite = document.querySelector('#jambeDroite');

/* Je déclare mes fonctions ----------------------------------------------------------------------------- */
/* Fonction permettant de mettre à zéro les animations */
var reset = function() {
    brasGauche.style.animation = "none";
    tete.style.animation = "none";
    bouche.style.animation = "none";
    bouche2.style.animation = "none";
    bulle.style.display = "none";
}
var resetVetements = function() {
    tronc.style.background = "white";
    brasDroit.style.background = "white";
    brasGauche.style.background = "white";
    /* Affichage de la bulle de texte */
    bouche.style.animation = "boucheParle 0.7s 1";
    bouche2.style.animation = "boucheParle 0.7s 1";
    bulle.style.display = "flex";
    bulle.textContent = "Ok on recommence...";
    /* Après 2 secondes je met à zero mes animations pour permettre de les refaires */
    setTimeout(reset, 2000);
}
/* Le robot salut du bras et incline sa tête, trop mignon ! - exercice 1.1 */
var salut = function() {
    brasGauche.style.animation = "brasBouge 2s";
    tete.style.animation = "teteBouge 2s";
    /* Affichage de la bulle de texte */
    bouche.style.animation = "boucheParle 0.7s 1";
    bouche2.style.animation = "boucheParle 0.7s 1";
    bulle.style.display = "flex";
    bulle.textContent = "Salut !";
    /* Après 2 secondes je met à zero mes animations pour permettre de les refaires */
    setTimeout(reset, 2000);
};
/* Inversion du contenu des pieds / Changer le contenu d'un texte - exercice 1.2*/
var inversionLacets = function() {
    piedsGauche.innerHTML = lacet2;
    piedsDroit.innerHTML = lacet1;
    /* Affichage de la bulle de texte */
    bouche.style.animation = "boucheParle 0.5s 2";
    bouche2.style.animation = "boucheParle 0.5s 2";
    bulle.style.display = "flex"; /* Changer une valeur CSS - exercice 1.3 */
    bulle.textContent = "Regarde mes lacets !";
    /* Après 2 secondes je met à zero mes animations pour permettre de les refaires */
    setTimeout(reset, 2000);
};
/* Je change la couleur de plusieurs éléments sans utiliser leurs ID - exercice 2.2 */
var changeBras = function() {
    brasGauche.style.background = "antiquewhite";
    tronc.style.background = "antiquewhite";
    brasDroit.style.background = "antiquewhite";
    /* Affichage de la bulle de texte */
    bouche.style.animation = "boucheParle 0.5s 2";
    bouche2.style.animation = "boucheParle 0.5s 2";
    bulle.style.display = "flex"; /* Changer une valeur CSS - exercice 1.3 */
    bulle.textContent = "top mon nouveau pull !";
    /* Après 2 secondes je met à zero mes animations pour permettre de les refaires */
    setTimeout(reset, 2000);
};
/* Je creer un short à mon robot - exercice 3*/
var creerShort = function(){
    var testShort = "";








    function theTest(val) {
        var answer = "";
        switch( val ) {
          case 1: case 2: case 3:
            answer = "Low";
            break;
          case 4: case 5: case 6:
            answer = "Mid";
            break;
          case 7: case 8: case 9:
            answer = "High";
            break;
          default:
            answer = "Massive or Tiny?";
        } 
        return answer;  
        }
      








    if (testShort=0){
        var shortGauche = document.createElement("div");
        shortGauche.className = "short";
        var shortDroit = document.createElement("div");
        shortDroit.className = "short";
        /* Je place les deux éléments du short sur les deux jambes */
        jambeGauche.appendChild(shortGauche);
        jambeDroite.appendChild(shortDroit);
        /* Affichage de la bulle de texte */
        bouche.style.animation = "boucheParle 0.5s 2";
        bouche2.style.animation = "boucheParle 0.5s 2";
        bulle.style.display = "flex";
        bulle.textContent = "J'adore ce short !";
        setTimeout(reset, 2000);
        testShort=1;
    }
    if (testShort=1){
        var shortGauche = document.createElement("div");
        shortGauche.className = "short";
        var shortDroit = document.createElement("div");
        shortDroit.className = "short";
        /* Je place les deux éléments du short sur les deux jambes */
        jambeGauche.appendChild(shortGauche);
        jambeDroite.appendChild(shortDroit);
        /* Affichage de la bulle de texte */
        bouche.style.animation = "boucheParle 0.5s 2";
        bouche2.style.animation = "boucheParle 0.5s 2";
        bulle.style.display = "flex";
        bulle.textContent = "Un pantalon c'est mieux !";
        setTimeout(reset, 2000);
        testShort=2;
    }
    else {
        return;
    }
}

/* J'appelle mes fonctions avec un clic ------------------------------------------------------------------ */
brasGauche.addEventListener('click', salut);
piedsGauche.addEventListener('click', inversionLacets);
brasDroit.addEventListener('click', changeBras);
cou.addEventListener('click', resetVetements);
jambeDroite.addEventListener('click', creerShort);



