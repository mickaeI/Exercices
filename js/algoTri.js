/* easy money ! 

var monTableau = ['19', '5000', '30000', '53', '10000'];

function compareNombres(a, b) {
    return a - b;
  }

monTableau.sort(compareNombres);
console.log(monTableau);

*/

// Affichage utilisateur ---------------------------------------------------------------------------------
var divDepart = document.querySelectorAll('div')[0];
var monTableau = [1,34,2,712,23,98,7,35,9,67,43];
divDepart.innerHTML="Tableau de départ : " + monTableau+"<br>";

var divFinal = document.querySelectorAll('div')[1];
var bouton = document.querySelector('button');
var actionBouton = function (){
  var tableauTrie = triRapide(monTableau);
  divFinal.innerHTML = "Tableau trié : "+tableauTrie;
}
bouton.addEventListener('click', actionBouton);

// Algorithme --------------------------------------------------------------------------------------------
// Fonction tri rapide
function triRapide(monTableau) {
  // Je vérifie que mon tableau comporte plus de deux cases
	if (monTableau.length <= 1) { 
		return monTableau;
	} else {
    // Je créer deux tableau, un de gauche et un de droite
		var tableauGauche = [];
    var tableauDroite = [];
    // Je créer un tableau qui recevra les deux précédent pour l'affichage du résultat
    var tableauTrie = [];
    // pivot = dernier élément du tableau
		var pivot = monTableau.pop();
		var length = monTableau.length;
    // Je teste les cases une par une
		for (var i = 0; i < length; i++) {
			if (monTableau[i] <= pivot) {
				tableauGauche.push(monTableau[i]); // si la case en question est inférieure à mon pivot je la met à gauche
			} else {
				tableauDroite.push(monTableau[i]); // si la case en question est supérieure à mon pivot je la met à droite
      }
    }
    console.log("tableauGauche : "+tableauGauche);
    console.log("tableauDroite : "+tableauDroite);
    console.log("pivot : "+pivot);
    
    // Je fusionne mes deux tableau, celui de gauche et celui de droite
    return tableauTrie.concat(triRapide(tableauGauche), pivot, triRapide(tableauDroite));
  }
}