// Elements de contrôle et d'affichage
var divAge = document.querySelectorAll('div')[0];
var divPicole = document.querySelectorAll('div')[1];
var divVieux = document.querySelectorAll('div')[2];
var boutonPicole = document.querySelector('#picole');
var boutonTempsPasse = document.querySelector('#tempsPasse');
var boutonReset = document.querySelector('#reset');
// Variables utiles pour les conditions
var age = 19;
var picole = false;
// Conditions
var picole = function (){
    if (picole != true){
        picole = true;
        divPicole.textContent = "Vous buvez et/ou fumez régulièrement";
    }
    else {
        picole = false;
        divPicole.textContent = "Vous ne buvez pas et ne fumez pas";
    }
}
var tempsPasse = function (){
    if (picole == false){
        age++;
    }
    else {
        age=age+2;
    }
    divAge.textContent = "Vous semblez avoir "+age+" ans";
    // Test vieillesse
    if (age > 25){
        divVieux.textContent = "Vous êtes vieux ! déso :'(";
    }
}
var reset = function (){
    picole = false;
    age = 19;
    divPicole.textContent = "Vous ne buvez pas et ne fumez pas";
    divAge.textContent = "Vous semblez avoir "+age+" ans";
    divVieux.textContent = "C'est bon vous êtes jeune !";
}
// Actions des boutons
boutonPicole.addEventListener('click', picole);
boutonTempsPasse.addEventListener('click', tempsPasse);
boutonReset.addEventListener('click', reset);